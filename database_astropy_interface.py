import numpy as np
from astropy.io import fits
from astropy import units as u
from astropy.coordinates import SkyCoord 

from app import db
from app.models import Candidate, Review, User 

def databaseDetectionsToSkyCoords(mode='positive'):
    if mode == 'positive':
      candidates = db.session.query(Candidate).filter(Candidate.humanlens>2*Candidate.humannope).all()
    if candidates:
      ra, dec  = zip(*[(c.ra/1e4, c.dec/1e4) for c in candidates])
      ids      = [c.id for c in candidates]
      skycoord = SkyCoord(ra=ra*u.degree, dec=dec*u.degree)
      return ids, skycoord
    else:
      return None, None

def databaseNegativesToSkyCoords():
    candidates = db.session.query(Candidate).filter(2*Candidate.humanlens<Candidate.humannope).all()
    if candidates:
      ra, dec  = zip(*[(c.ra/1e4, c.dec/1e4) for c in candidates])
      ids      = [c.id for c in candidates]
      skycoord = SkyCoord(ra=ra*u.degree, dec=dec*u.degree)
      return ids, skycoord
    else:
      return None, None

def fitsToSkyCoords(fitspath):
    cat      = fits.getdata(fitspath)
    cat_ra   = np.array([e[0] for e in cat])
    cat_dec  = np.array([e[1] for e in cat])
    skycoord = SkyCoord(ra=cat_ra*u.degree, dec=cat_dec*u.degree)
    return cat, skycoord

def matchCatalogues(small, bigger, threshold=1e-3):
    idx, d2d, _   = small.match_to_catalog_sky(bigger)
    matches       = np.where(d2d<threshold*u.degree)
    idx, d2d      = idx[matches], d2d[matches]
    return idx, d2d

def identifyPotentialNewMatches(fitsdatabase='lensedquasars.fits', mode='positive'):
    _, catalogue_cameron = fitsToSkyCoords(fitsdatabase)
    if mode == 'positive':
      ids, skycoords    = databaseDetectionsToSkyCoords()
    else:
      ids, skycoords    = databaseNegativesToSkyCoords()

    idx, _            = matchCatalogues(catalogue_cameron, skycoords)
    # alright, idx are now positions of matches inside skycoords, or ids.
    # we want all the others. 
    mask = ~np.in1d(np.arange(len(skycoords)), idx)
    nonmatches = np.where(mask)[0]
    # and we should have a list of new "detections" not found in
    # the original catalogue.
    # we return the primary keys to our database:
    ids = np.array(ids)
    return ids[idx], ids[nonmatches]


def fillScoresForCandidates(filename):
  with open(filename, 'r') as f:
    for line in f.readlines():
      line = line.strip()
      num, score = line.split('\t')
      num = num.split('_')[0]
      score = float(score)
      #if not score > 5:
      #  continue
      c = db.session.query(Candidate).filter(Candidate.filename.startswith(num)).all()
      if len(c) > 1:
        print(f"################## too many candidates with {num} ##################")
      elif len(c) == 0:
        print(f"################## no candidate with {num} ##################")
      else:
        # pass
        c = c[0]
        if c.diff_score:
          print(f'wow, {c.filename} has a diffscore')
        score = -1 if score > 5 else score
        c.diff_score = score 
        db.session.add(c)
        db.session.commit() 
        print(f'added {score} to {c}')
      


if __name__ == "__main__":
    #m, nm = identifyPotentialNewMatches()
    fillScoresForCandidates('/home/frederic/Nextcloud/diffpanstarrs/tests/scores2')


    