import pandas as pd
from os.path import basename 
import multiprocessing
from app.models import Candidate
from app import db

from download_stamps import getJpgStamps


CSV_catalogue_path  = 'scores_first_part_miliquas_i=16_positivesonly.csv'
rho_cnn_lim         = 0.5


CSV_catalogue = pd.read_csv(CSV_catalogue_path)
#positives = CSV_catalogue[CSV_catalogue['score'] > rho_cnn_lim]
positives = CSV_catalogue

comment = 'quad_candidate_gri_cnn'





def actual_download(row):
    try:    
        ra, dec = int(1e4*row['RA']), int(1e4*row['DEC'])

        candidate = db.session.query(Candidate).filter(Candidate.ra < ra + 10).filter(Candidate.ra > ra - 10)
        if candidate.count():
            candidate = candidate.filter(Candidate.dec < dec + 10).filter(Candidate.dec > dec - 10)
        if candidate.count():
            candidate = candidate.first() 
        else:
            candidate = Candidate(ra=ra, dec=dec, humanlens=0, humanbroken=0, humannope=0,\
                                    filename='')
        if candidate.comment == comment:
            return

        candidate.comment = comment
        candidate.grid_reviewed_count = 0
        candidate.cherry_picked_count = 0
        candidate.humannope = 0 
        candidate.quad_score = float(row['score'])
        db.session.add(candidate)
        db.session.commit()
        newpath = getJpgStamps(candidate, outdir='app/static/stamps_from_miliquas/')
        candidate.miliquas_stamp = basename(newpath)
        print(f'adding {candidate}')
        
        db.session.add(candidate)
        db.session.commit()
    except:
        pass

rows = [positives.iloc[i] for i in range(len(positives))]

pool = multiprocessing.Pool(10)
pool.map(actual_download, rows)
