#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Nov 12 17:52:32 2020

@author: fred
"""

import ast

import operator as op

# supported operators
operators = {ast.Add: op.add, ast.Sub: op.sub, ast.Mult: op.mul,
             ast.Div: op.truediv, ast.Pow: op.pow, ast.BitXor: op.xor,
             ast.USub: op.neg}

numbers = {'0':'⁰', 
           '1':'¹',
           '2':'²',
           '3':'³',
           '4':'⁴',
           '5':'⁵',
           '6':'⁶',
           '7':'⁷',
           '8':'8',
           '9':'⁹',
           '-':'⁻',
           '+':'⁺'
           }


def eval_expr(a):
    """
    >>> eval_expr('2^6')
    4
    >>> eval_expr('2**6')
    64
    >>> eval_expr('1 + 2*3**(4^5) / (6 + -7)')
    -5.0
    """
    a = a.replace("'", "").replace("^", "**").replace(",", ".").replace("%", "/").replace('x', '*')
    a = a.replace(':', '/')
    while "  " in a:
        a = a.replace("  ", " ")
    for s in ["/", "^", "'", ".", "**", "x", "*", "+", "-", ")", "(", ":"]:
        while f" {s}" in a:
            a = a.replace(f" {s}", f"{s}")
        while f"{s} " in a:
            a = a.replace(f"{s} ", f"{s}")
        
    a = a.replace(" ", "*")
    
    num = eval_(ast.parse(a, mode='eval').body)
    
    expr = f"{num:.12E}"
    m, e = expr.split('E')
    while  len(m)>1 and m[-1] == '0':
        m = m[:-1]
    while len(e) > 2 and e[1] == '0':
        e = e[0]+e[2:]
    if e[0] == "+":
        e = e[1:]

    if m.endswith('.'):
        m = m[:-1]
    e_better = ''
    for i in range(len(e)):
        if e[i] in numbers.keys():
            e_better+= numbers[e[i]]
        else:
            e_better += e[i]
    expr = m + "·10"+e_better
    return expr


def eval_(node):
    if isinstance(node, ast.Num): # <number>
        return node.n
    elif isinstance(node, ast.BinOp): # <left> <operator> <right>
        return operators[type(node.op)](eval_(node.left), eval_(node.right))
    elif isinstance(node, ast.UnaryOp): # <operator> <operand> e.g., -1
        return operators[type(node.op)](eval_(node.operand))
    else:
        raise TypeError(node)

