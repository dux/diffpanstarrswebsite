from shutil import move, copyfile
from app.models import Candidate
from app import db
from os.path import basename
from glob import glob
from os.path import join
import pandas as pd


a = glob('/home/ubuntu/new_miliquas_stamps/*jpg')
#a = glob('app/static/stamps_from_miliquas/*jpg')
dest = 'app/static/stamps_from_miliquas/'

for path in a:
        filename = basename(path)
        splits   = filename.replace('.jpg', '').split('_')
        recno    = int(splits[1])
        ra, dec  = splits[2:4]
        RA, DEC  = int(1e4*float(ra)), int(1e4*float(dec))
        imag, rmag = splits[4:6]
        iKmag, rKmag = splits[6:8]
        imag, rmag  = float(imag), float(rmag)
        iKmag, rKmag = float(iKmag), float(rKmag)
        print(recno, ra, dec, imag, rmag, iKmag, rKmag)
        if not a:
            import sys
            sys.exit()
        candidate = db.session.query(Candidate).filter(Candidate.miliquas_recno == recno).first()
        if not candidate:
            candidate = Candidate(ra=RA, dec=DEC, humanlens=0, humanbroken=0, humannope=0,\
                                  filename='')
        candidate.miliquas_recno = recno
        candidate.miliquas_imag  = imag
        candidate.miliquas_rmag  = rmag
        candidate.miliquas_iKmag = iKmag
        candidate.miliquas_rKmag = rKmag
        candidate.miliquas_stamp = filename
        candidate.comment        = "single_panstarrs_cut_0.3_mag_19.5-20.5"

        db.session.add(candidate)
        newpath = join(dest, filename)
        move(path, newpath)
db.session.commit()
