from flask import render_template, flash, redirect, url_for, request
from flask_login import current_user, login_user, logout_user,\
                        login_required
from werkzeug.urls import url_parse

from glob import glob
from os.path import join, basename

from random import choice
import csv
from io import StringIO
from werkzeug.wrappers import Response


from app import app
from app import db
from app.forms import LoginForm, RegistrationForm, ResultSelectionForm, inputStringForm
from app.models import User, Review, Candidate
from database_astropy_interface import identifyPotentialNewMatches
from eval_expr import eval_expr

@app.route('/')
@app.route('/index')
def index():
  Nreview = db.session.query(Review).count()
  Nusers  = db.session.query(User).count()
  totallens = db.session.query(Candidate).filter(Candidate.filename!='').count()
  if current_user.is_authenticated:
    username = current_user.username
  else:
    username = 42*"None"
  user_from_database = db.session.query(User).filter(User.username == username).first()
  if user_from_database:
    Ncurrent = user_from_database.reviews.count()
  else:
    Ncurrent = 0
  return render_template('index.html', title='Lensed Quasars', nreview=Nreview, nusers=Nusers, ncurrent=Ncurrent, totallens=totallens)

@app.route('/false_negatives',  methods=['GET', 'POST'])
@login_required
def false_negatives():
  matches, _ = identifyPotentialNewMatches(fitsdatabase='lensedquasars.fits', mode='negative')
  matchurls, matchcandidates = [], []
  filenamesmatch, stampsmatch     = [], []
  for match in matches:
    candidate = db.session.query(Candidate).filter(Candidate.id == int(match)).first()
    matchurls.append(makePanstarrsURL(candidate.ra/1e4, candidate.dec/1e4))
    matchcandidates.append(candidate)
    stampsmatch.append(url_for("static", filename=f"stamps/colorstamp_{candidate.id}_{candidate.ra}_{candidate.dec}.jpg"))
    image = candidate.filename
    image = url_for("static", filename="images2/diffpanstarrsoutput/"+image)
    filenamesmatch.append(image)
  return render_template('false_negatives.html', title='Results',
                         cand_url_file_match=zip(matchcandidates, matchurls, filenamesmatch, stampsmatch))


@app.route('/miliquas_results',  methods=['GET', 'POST'])
@login_required
def miliquas_results():
    form = ResultSelectionForm()
    if form.validate_on_submit():
        onlyupvoted = form.onlyupvoted.data
        onlydownvoted = form.onlydownvoted.data
        notmanual  = form.notmanual.data
        scorelimit = form.scorelimit.data
        rmagmin = form.rmagmin.data
        rmagmax = form.rmagmax.data
        cherrypicked = form.cherrypicked.data
        notcherrypicked = form.notcherrypicked.data
        onlykronpsf = form.onlykronpsf.data
        onlyquadcandidates = form.onlyquadcandidates.data 
        onlyquadcandidates2 = form.onlyquadcandidates2.data
        candidates = db.session.query(Candidate)
        candidates = candidates.filter(Candidate.miliquas_stamp != None)
        if form.RA.data:
            ra = int(1e4*float(form.RA.data))
            candidates = candidates.filter(Candidate.ra>ra-100)
            candidates = candidates.filter(Candidate.ra<ra+100)
        if form.DEC.data:
            dec = int(1e4*float(form.DEC.data))
            candidates = candidates.filter(Candidate.dec>dec-100)
            candidates = candidates.filter(Candidate.dec<dec+100)
        if onlyupvoted:
            candidates = candidates.filter(Candidate.humanlens>0)
        if onlydownvoted:
            candidates = candidates.filter(Candidate.humannope>0)
        if notmanual:
            candidates = candidates.filter(Candidate.totalreviews==0)
        if rmagmin:
            candidates = candidates.filter(Candidate.miliquas_rmag>=float(rmagmin))
        if rmagmax:
            candidates = candidates.filter(Candidate.miliquas_rmag<=float(rmagmax))
        if scorelimit:
            try:
                if scorelimit[0] == '>':
                    candidates = candidates.filter(Candidate.diff_score > float(scorelimit[1:]))
                elif scorelimit[0] == '<':
                    candidates = candidates.filter(Candidate.diff_score < float(scorelimit[1:]))
            except:
                flash(f"Error with the score limit: could not process {scorelimit}")
        if cherrypicked:
            candidates = candidates.filter(Candidate.cherry_picked_count>0)
        if notcherrypicked:
            candidates = candidates.filter(Candidate.cherry_picked_count == 0)
        if onlykronpsf:
            candidates = candidates.filter(Candidate.comment != "")
        if onlyquadcandidates:
            candidates = candidates.filter(Candidate.comment == 'quad_candidate_gri_cnn')
            candidates = candidates.order_by(Candidate.quad_score.desc()).all()
        elif onlyquadcandidates2:
            candidates = candidates.filter(Candidate.comment == 'quad_candidate_gri_cnn_v2')
            candidates = candidates.order_by(Candidate.quad_score.desc()).all()
        else:
            candidates = candidates.order_by(Candidate.diff_score.desc()).all()
    else:
        candidates = []

    upvote, downvote, requestdiff = request.form.get('upvote'), request.form.get('downvote'), request.form.get('requestdiff')
    print("\n\n\nFORM:", request.form, "\n\n\n")
    if upvote or downvote or requestdiff:
      for e in request.form:
          if 'vote' in e:
              continue
          try:
              id = int(e)
              if requestdiff:
                  cand = db.session.query(Candidate).filter(Candidate.id==id).first()
                  print(cand)
                  cand.cherry_picked_count += 1
                  db.session.add(cand)
                  db.session.commit()
              else:
                  upvoteDownvote(upvote, downvote, id, None)
          except Exception as ex:
              print(ex, e)
    return render_template('miliquas_results.html', title='Miliquas Results', \
                            cands=candidates, form=form)


@app.route('/negative_results',  methods=['GET', 'POST'])
@login_required
def negative_results():
  matches, _ = identifyPotentialNewMatches(fitsdatabase='contaminants.fits', mode='negative')
  matchurls, matchcandidates = [], []
  filenamesmatch, stampsmatch     = [], []
  for match in matches:
    candidate = db.session.query(Candidate).filter(Candidate.id == int(match)).first()
    matchurls.append(makePanstarrsURL(candidate.ra/1e4, candidate.dec/1e4))
    matchcandidates.append(candidate)
    stampsmatch.append(url_for("static", filename=f"stamps/colorstamp_{candidate.id}_{candidate.ra}_{candidate.dec}.jpg"))
    image = candidate.filename
    image = url_for("static", filename="images2/diffpanstarrsoutput/"+image)
    filenamesmatch.append(image)
  return render_template('negative_results.html', title='Results',
                         cand_url_file_match=zip(matchcandidates, matchurls, filenamesmatch, stampsmatch))

@app.route('/results',  methods=['GET', 'POST'])
@login_required
def results():
  matches, nonmatches = identifyPotentialNewMatches()
  matchurls, matchcandidates = [], []
  filenamesmatch, stampsmatch     = [], []
  nonmatchurls, nonmatchcandidates = [], []
  filenamesnonmatch, stampsnonmatch      =  [], []
  for match in matches:
    candidate = db.session.query(Candidate).filter(Candidate.id == int(match)).first()
    matchurls.append(makePanstarrsURL(candidate.ra/1e4, candidate.dec/1e4))
    matchcandidates.append(candidate)
    stampsmatch.append(url_for("static", filename=f"stamps/colorstamp_{candidate.id}_{candidate.ra}_{candidate.dec}.jpg"))
    image = candidate.filename
    image = url_for("static", filename="images2/diffpanstarrsoutput/"+image)
    filenamesmatch.append(image)
  for nonmatch in nonmatches:
    candidate = db.session.query(Candidate).filter(Candidate.id == int(nonmatch)).first()
    nonmatchurls.append(makePanstarrsURL(candidate.ra/1e4, candidate.dec/1e4))
    nonmatchcandidates.append(candidate)
    stampsnonmatch.append(url_for("static", filename=f"stamps/colorstamp_{candidate.id}_{candidate.ra}_{candidate.dec}.jpg"))
    image = candidate.filename
    image = url_for("static", filename="images2/diffpanstarrsoutput/"+image)
    filenamesnonmatch.append(image)

  upvote, downvote = request.form.get('upvote'), request.form.get('downvote')
  if upvote or downvote:
    if upvote:
        id = request.form.get('upvote')
    else:
        id = request.form.get('downvote')
    response =  upvoteDownvote(request.form.get('upvote'), request.form.get('downvote'), id, results)
    if response:
      return response

      # if rev_to_delete:
      #   res = rev_to_delete.review
      #   candidate = rev_to_delete.candidate
      #   current  = getattr(candidate, res)
      #   setattr(candidate, res, current-1)
      #   db.session.delete(rev_to_delete)
      #   db.session.add(candidate)
      #   db.session.commit()
      #   return personal_page()
  return render_template('results.html', title='Results',
                         cand_url_file_match=zip(matchcandidates, matchurls, filenamesmatch, stampsmatch),
                         cand_url_file_nonmatch=zip(nonmatchcandidates, nonmatchurls, filenamesnonmatch,stampsnonmatch))


def upvoteDownvote(upvote, downvote, id, redirect):
  if upvote:
      cand = db.session.query(Candidate).filter(Candidate.id==id).first()
      rev  = cand.reviews.filter(Review.author == current_user).first()
      if rev and rev.review != 'humanlens':
        setattr(cand, rev.review, getattr(cand, rev.review)-1)
        rev.review = 'humanlens'
        cand.humanlens += 1
        db.session.add(cand)
        db.session.add(rev)
        db.session.commit()
        if redirect:
            return redirect()
      elif rev == None:
        rev = Review(author=current_user, review='humanlens', candidate=cand)
        current  = getattr(cand, 'humanlens')
        setattr(cand, 'humanlens', current+1)
        db.session.add(cand)
        db.session.add(rev)
        db.session.commit()
        if redirect:
            return redirect()
  if downvote:
      cand = db.session.query(Candidate).filter(Candidate.id==id).first()
      rev  = cand.reviews.filter(Review.author == current_user).first()
      if rev and rev.review == 'humanlens':
        rev.review = 'humannope'
        cand.humannope += 1
        cand.humanlens -= 1
        db.session.add(cand)
        db.session.add(rev)
        db.session.commit()
        if redirect:
            return redirect()
      elif rev == None:
        rev = Review(author=current_user, review='humannope', candidate=cand)
        current  = getattr(cand, 'humannope')
        setattr(cand, 'humannope', current+1)
        db.session.add(cand)
        db.session.add(rev)
        db.session.commit()
        if redirect:
            return redirect()


@app.route('/csv')
@login_required
def download_log():
    def generate():
        data = StringIO()
        w = csv.writer(data)

        # write header
        w.writerow(('filename', 'RA', 'DEC', 'Lens', 'Unsure', 'Not_lens'))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)

        # write each log item
        for item in db.session.query(Candidate).filter(Candidate.totalreviews>0).all():
            w.writerow((
                str(item.filename),
                str(item.ra/1e4),
                str(item.dec/1e4),
                str(item.humanlens),
                str(item.humanbroken),
                str(item.humannope)
            ))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)

    # stream the response as the data is generated
    response = Response(generate(), mimetype='text/csv')
    # add a filename
    response.headers.set("Content-Disposition", "attachment", filename="diffpanstarrs_classified.csv")
    return response



@app.route('/needing_diff_img')
def list_needing_diffimg():
    def generate():
        data = StringIO()
        w = csv.writer(data)

        # write header
        w.writerow(('databaseid', 'RA', 'DEC'))
        yield data.getvalue()
        data.seek(0)
        data.truncate(0)
        #items = db.session.query(Candidate).filter(Candidate.cherry_picked_count>0).filter(Candidate.filename=='').all()
        items = db.session.query(Candidate).filter(Candidate.comment=="quad_candidate_gri_cnn_v2").filter(Candidate.cherry_picked_count>0).filter(Candidate.filename=='').order_by(Candidate.cherry_picked_count.desc()).all()
        # write each log item
        for item in items:
            w.writerow((
                str(item.id),
                str(item.ra/1e4),
                str(item.dec/1e4),
            ))
            yield data.getvalue()
            data.seek(0)
            data.truncate(0)

    # stream the response as the data is generated
    response = Response(generate(), mimetype='text/csv')
    # add a filename
    response.headers.set("Content-Disposition", "attachment", filename="needing_diffpanstarrs.csv")
    return response


@app.route('/tutorial')
def tutorial():
  if not current_user.is_authenticated:
    return redirect(url_for('login'))
  return render_template('tutorial.html', title="tutorial")

@app.route('/login', methods=['GET', 'POST'])
def login():
  if current_user.is_authenticated:
    return redirect(url_for('index'))
  form = LoginForm()
  if form.validate_on_submit():
    user = User.query.filter_by(username=form.username.data).first()
    if user is None or not user.check_password(form.password.data):
      flash('Invalid username or password')
      return redirect(url_for('login'))
    login_user(user, remember=form.remember_me.data)
    next_page = request.args.get('next')
    if not next_page or url_parse(next_page).netloc != '':
      return redirect(url_for('index'))
    return redirect(next_page)
  return render_template('login.html', title="Sign in", form=form)

@app.route('/logout')
def logout():
  logout_user()
  return redirect(url_for('index'))

@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data,\
                    email=form.email.data)
        user.set_password(form.password.data)
        # DISABLE REGISTER
        #db.session.add(user)
        #db.session.commit()
        #flash(f"Congratulations {user.username}, you are now a registered user")
        flash(f"Sign up disabled")
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)


@app.route('/personal_page', methods=['POST', 'GET'])
@login_required
def personal_page():
  reviews = current_user.reviews.order_by(Review.timestamp.desc()).limit(10)
  urls     = []
  stamps   = []
  for rev in reviews:
    RA, DEC = rev.candidate.ra/1e4, rev.candidate.dec/1e4
    urls.append(makePanstarrsURL(RA, DEC))
    stamps.append( url_for("static", filename=f"stamps/colorstamp_{rev.candidate.id}_{rev.candidate.ra}_{rev.candidate.dec}.jpg") )
  if request.form.get('delete'):
      id = request.form.get('delete')
      rev_to_delete = db.session.query(Review).filter(Review.id==id).first()
      if rev_to_delete:
        res = rev_to_delete.review
        candidate = rev_to_delete.candidate
        current  = getattr(candidate, res)
        setattr(candidate, res, current-1)
        db.session.delete(rev_to_delete)
        db.session.add(candidate)
        db.session.commit()
        return personal_page()
  return render_template('personal_page.html', reviews_urls=zip(reviews, urls, stamps))

def makePanstarrsURL(RA, DEC):
  return f"https://ps1images.stsci.edu/cgi-bin/ps1cutouts?pos={RA}%2C+{DEC}&filter=color&filter=g&filter=r&filter=i&filetypes=stack&auxiliary=data&size=40&output_size=0&verbose=0&autoscale=99.500000&catlist="

@app.route('/protected')
@login_required
def protected():
    # check the database:
    candidate = db.session.query(Candidate, Review.id).\
                       outerjoin(Review, Review.cand_id == Candidate.id).\
                       filter(Review.id.is_(None)).\
                       outerjoin(User, Review.user_id == User.id).\
                       filter(Review.id.is_(None)).\
                       filter(Candidate.miliquas_iloc == None).\
                       order_by(Candidate.totalreviews, Candidate.diff_score.desc(), Candidate.filename).first()

    candidate = candidate[0]
    print(candidate.diff_score)
    RA, DEC = candidate.ra/1e4, candidate.dec/1e4
    image   = candidate.filename
    image   = url_for("static", filename="images2/diffpanstarrsoutput/"+image)
    stamp   = url_for("static", filename=f"stamps/colorstamp_{candidate.id}_{candidate.ra}_{candidate.dec}.jpg")
    url     = makePanstarrsURL(RA, DEC)
    return render_template('protected.html', title='Classify', image_to_display=image, url=url, stamp=stamp)

@app.route("/forward/", methods=['POST'])
@login_required
def move_forward():

    if request.form.get('lens'):
        res = 'humanlens'
        image = request.form.get('lens')
    elif request.form.get('broken'):
        res = 'humanbroken'
        image = request.form.get('broken')
    elif request.form.get('nope'):
        res = 'humannope'
        image = request.form.get('nope')

    image = basename(image)
    RA, DEC = image.split('_')[1:3]

    RA, DEC   = int(1e4*float(RA)), int(1e4*float(DEC))
    candidate = db.session.query(Candidate).filter_by(ra=RA, dec=DEC).first()
    rev       = Review(author=current_user, review=res, candidate=candidate)
    current  = getattr(candidate, res)
    setattr(candidate, res, current+1)
    db.session.add(candidate)
    db.session.add(rev)
    db.session.commit()
    return protected()


@app.route("/cherry_picking/", methods=['GET','POST'])
@login_required
def cherry_picking():
    # part where we select candidates for display.
    gridsize = 9 * 9
    cands = db.session.query(Candidate).filter(Candidate.miliquas_stamp != "")\
                                       .filter(Candidate.comment == "quad_candidate_gri_cnn_v2")\
                                       .filter(Candidate.quad_score is not None)
                                       
    ntot  = cands.count()
    ndone = cands.filter(Candidate.grid_reviewed_count >= 1).count()
    cands = cands.filter(Candidate.grid_reviewed_count < 1)
    cands = cands.order_by(Candidate.quad_score.desc()).limit(gridsize).all()
     
    Ncands = l =  len(cands)
    for cand in cands:
        print(cand.quad_score)
    if Ncands<81:
        for _ in range(81-Ncands):
            cands.append( Candidate(ra=0, dec=0, humanlens=0, humanbroken=0, humannope=0, filename='nope',
                                    miliquas_stamp="nope", quad_score=-100))


    # part where we read the grid.
    data = request.form
    if len(data)>0:
        for e in data:
            cand = db.session.query(Candidate).filter(Candidate.id==int(e)).first()
            cand.grid_reviewed_count += 1
            if int(data[e]):
               if current_user.username == 'cameron':
                     cand.cherry_picked_count += 3
               else:
                     cand.cherry_picked_count += 1
               db.session.add(cand) 
        db.session.commit()


    resp = render_template('cherry_picking.html', cands=cands, ntot=ntot, ndone=ndone)
    if request.method == 'POST':
        return redirect(url_for('cherry_picking'))
    return resp



from matplotlib.backends.backend_agg import FigureCanvasAgg as FigureCanvas
import matplotlib.pyplot as plt 
import numpy as np 
import io
@app.route("/gallery.png/")
def plot_png():
    fig = plt.figure()
    fig, axs = plt.subplots(5, 5, figsize=(10,10))
    axs = axs.flatten()
    N = 7
    dirs = '/home/ubuntu/gallery_train_cnn/'
    dirpos = join(dirs, 'poss')
    dirneg = join(dirs, 'negs')
    files = glob(join(dirneg, '*npy'))
    filesp= glob(join(dirpos, '*npy'))
    for i in range(15):
        cho = choice(files)
        print(cho)
        im = np.load(cho)[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        axs[i].imshow(im, interpolation='nearest', origin='lower')
        axs[i].set_xticks([])
        axs[i].set_yticks([])
    for i in range(15, 25):
        im = np.load(choice(filesp))[5:-5, 5:-5, :]
        m, s = np.nanmean(im), np.nanstd(im)
        im[im>m+N*s] = m+N*s
        im -= np.nanmin(im)
        im /= np.nanmax(im)
        axs[i].imshow(im, interpolation='nearest', origin='lower')
        axs[i].set_xticks([])
        axs[i].set_yticks([])
    plt.tight_layout()
    output = io.BytesIO()
    FigureCanvas(fig).print_png(output)
    return Response(output.getvalue(), mimetype='image/png')
@app.route("/pour_giuliano/", methods=['GET', 'POST'])
def pour_giuliano():
    s = "0"
    data = inputStringForm()
    if data.validate_on_submit():
        s = data.field.data
    try:
        answer = eval_expr(s)
    except:
        answer = "Erreur :( essaie de modifier un peu l'entrée ?"
    return render_template("pour_giuliano.html",form=data, answer=answer)
