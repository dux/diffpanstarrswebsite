from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, DecimalField
from wtforms.validators import DataRequired, ValidationError, Email, EqualTo

from app.models import User


class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField('Repeat Password', \
                 validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_user(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user:
            raise ValidationError('This username is already used. Choose another one.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user:
            raise ValidationError('This email is already registered')


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember me')
    submit = SubmitField('Sign in')


class ResultSelectionForm(FlaskForm):
    onlyupvoted = BooleanField("Show only manually upvoted")
    onlydownvoted = BooleanField("Show only manually downvoted")
    notmanual  = BooleanField("Exclude those that were manually selected")
    scorelimit = StringField("Score range")
    rmagmin = StringField("r-mag min")
    rmagmax = StringField("r-mag max")
    RA, DEC  = StringField("RA"), StringField("DEC")
    cherrypicked = BooleanField("Show only cherry picked")
    notcherrypicked = BooleanField("Show only 'not' cherry picked")
    onlykronpsf = BooleanField("Show only KRON-PSF set")
    onlyquadcandidates = BooleanField("Show only CNN selected")
    onlyquadcandidates2 = BooleanField("Show only CNN selected (v2)")
    submit = SubmitField('Fetch')
class inputStringForm(FlaskForm):
    field = StringField("Une opération mathématique, par exemple '2 x 3 + 7 x 10^-3 / (5 x 10^5)'")
