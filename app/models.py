from app import db, login
from datetime import datetime
from werkzeug.security import check_password_hash, generate_password_hash
from flask_login import UserMixin

from astropy.coordinates import ICRS
from astropy import units

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

class User(UserMixin, db.Model):
    id       = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email    = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    reviews       = db.relationship('Review', backref='author', lazy='dynamic')
    # in db.relationship, use class names. Unlike in db.ForeignKey below
    # where use the sql table name.
    did_tutorial = db.Column(db.Boolean, default=False)
    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return f'<User {self.username}, id no {self.id}, with email {self.email}>'

class Candidate(db.Model):
    id           = db.Column(db.Integer, primary_key=True)
    ra           = db.Column(db.Integer)
    dec          = db.Column(db.Integer)
    humanlens    = db.Column(db.Integer)
    humanbroken  = db.Column(db.Integer)
    humannope    = db.Column(db.Integer)
    totalreviews = db.column_property(humanlens+humanbroken+humannope)
    timestamp    = db.Column(db.DateTime, default=datetime.utcnow)
    name         = db.Column(db.String(128))
    filename     = db.Column(db.String(128), default="")
    medianfilename = db.Column(db.String(128), default="")
    reviews      = db.relationship('Review', backref='candidate', lazy='dynamic')
    diff_score   = db.Column(db.Float, default=-100)
    quad_score   = db.Column(db.Float, default=-100)


    # now, this will link to the pandas dataframe resulting from the
    # cross matching of the MILIQUAS database and the pan-STARRS detections.
    miliquas_iloc  = db.Column(db.Integer, default=0)
    miliquas_stamp = db.Column(db.String(128), default="")
    miliquas_recno = db.Column(db.Integer, default=0)
    miliquas_imag  = db.Column(db.Float)
    miliquas_rmag  = db.Column(db.Float)
    miliquas_iKmag = db.Column(db.Float)
    miliquas_rKmag = db.Column(db.Float)
    miliquas_magKDiff = db.Column(db.Float)
    miliquas_magDiff = db.Column(db.Float)
    miliquas_riKColorIndexDiff = db.Column(db.Float)
    miliquas_riColorIndexDiff = db.Column(db.Float)
    miliquas_angular_separation = db.Column(db.Float)
    # the gri pictures will be displayed to the user in a grid. When selected,
    # the candidate will gain priority through this counter:
    cherry_picked_count = db.Column(db.Integer, default=0)
    grid_reviewed_count = db.Column(db.Integer, default=0)

    comment = db.Column(db.String(128), default="")

    def __repr__(self):
        ra, dec = self.ra/1e4, self.dec/1e4
        c = ICRS(ra*units.degree, dec*units.degree)
        coords = f"{str(c.ra):<15}  {str(c.dec):<15}"
        return f"Object at {coords}, classified {self.totalreviews} times."
    def __str__(self):
        return self.__repr__()

class Review(db.Model):
    id        = db.Column(db.Integer, primary_key=True)
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id   = db.Column(db.Integer, db.ForeignKey('user.id'))
    cand_id   = db.Column(db.Integer, db.ForeignKey('candidate.id'))
    review    = db.Column(db.String(64))

    def __repr__(self):
        ra, dec = self.candidate.ra/1e4, self.candidate.dec/1e4
        c = ICRS(ra*units.degree, dec*units.degree)
        coords = f"{str(c.ra):<15}  {str(c.dec):<15}"
        nicer = {'humanlens': 'a probable lens', 'humanbroken': 'broken data or unsure', 'humannope': 'probably not a lens'}
        return f"Object at {coords} determined to be: {nicer[self.review]}"
    def __str__(self):
        return self.__repr__()
