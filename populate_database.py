#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat May  9 23:53:30 2020

@author: frederic
"""

from os.path import join, basename, dirname, exists
from glob    import glob
from shutil  import move
from app import db
from app.models import Candidate
from subprocess import call
from download_stamps import getJpgStamps


def insertAvailable(newsimages, directory):
    images = glob(join(newsimages, '*png'))
    images = [image for image in images if not '_median_' in image]
    for imagepath in images:
        if 'miliquas' in imagepath:
            imdir = dirname(imagepath)
            image = basename(imagepath)
            _, databaseid, ra, dec, score = image.split('_')
            databaseid = int(databaseid)
            ra, dec = int(1e4*float(ra)), int(1e4*float(dec))
            score = float(score.replace('score=','').replace('.png', ''))
            found = db.session.query(Candidate).filter(Candidate.id==databaseid).first()
            found.filename = image
            medianfilename = image.replace('miliquas_', 'miliquas_median_')
            medianpath = join(imdir, medianfilename)
            if not exists(medianpath) and exists(imagepath):
                continue
            found.medianfilename = medianfilename 
            found.diff_score = score
            db.session.add(found)

            newpath = join(directory, image)
            move(imagepath, newpath) 
            move(join(imdir, medianfilename), join(directory, medianfilename))
            pathcrop = join(directory, 'crop_' + image)
            call(['convert', newpath, '-crop', '300x300+40+360', pathcrop])

            db.session.commit() 
""" 
        image  = basename(imagepath)
        RA, DEC, score = image.split('_')[1:4]
        RA, DEC, score = int(1e4*float(RA)), int(1e4*float(DEC)), float(score)
        found   = db.session.query(Candidate).filter_by(ra=RA, dec=DEC).first()
        move(imagepath, join(directory, image))
        if not found:
            print(image, 'not found in database, inserting it')
            candidate = Candidate(ra=RA, dec=DEC, humanlens=0, humanbroken=0, humannope=0,\
                                  filename=image, diff_score=score)
            move(imagepath, join(directory, image))
            db.session.add(candidate)
            db.session.commit()
            candidate = db.session.query(Candidate).filter(Candidate.ra==RA, Candidate.dec==DEC, Candidate.filename==image).first()
            getJpgStamps(candidate)

#archive
def insertNewQuasarInDataBase(ra, dec):
    conn = sqlite3.connect('detections.db')
    ra = int(1e4*float(ra))
    dec = int(1e4*float(dec))
    req = f'INSERT OR IGNORE INTO diffpanstarrsoutput(ra, dec, humanlens, humanbroken, humannope) VALUES({ra}, {dec}, 0,0,0)'
    conn.execute(req)
    
    
def insertInDataBase(ra, dec, column):
    conn = sqlite3.connect('detections.db')
    # verify if the row already exists:
    ra = int(1e4*float(ra))
    dec = int(1e4*float(dec))
    insertNewQuasarInDataBase(ra, dec)
    # now update the column:
    print('inserting\n\n\n\n!')
    req2 = f"update diffpanstarrsoutput set {column}={column}+1 where ra={ra} and dec={dec}"
    conn.execute(req2)
    conn.commit()
    conn.close()
"""

if __name__ == "__main__":
    from sys import argv
    origin = argv[1]
    destination = argv[2]
    insertAvailable(origin, destination)
    # import sqlite3
    # conn = sqlite3.connect('detections.db')
    # for row in conn.execute('select * from diffpanstarrsoutput'):
    #     ra, dec, humanlens, humanbroken, humannope, _ = row
    #     print(ra, dec, humanlens, humanbroken, humannope)
    #     cand = db.session.query(Candidate).filter_by(ra=ra, dec=dec).first()
    #     cand.humanlens = humanlens
    #     cand.humanbroken = humanbroken
    #     cand.humannope = humannope
    #     db.session.add(cand)
