#!/usr/bin/env python3

from os.path import join, exists
from shutil import move
from subprocess import call

from panstamps.downloader import downloader

from app import db
from app.models import Candidate
#from database_astropy_interface import findPositiveCandidates


from logging import Logger
logger = Logger('stampdownloader.log')

def getJpgStamps(candidate, outdir='app/static/stamps'):
    ra, dec = candidate.ra, candidate.dec
    newname = f"colorstamp_{candidate.id}_{ra}_{dec}.jpg"
    ra, dec = ra/1e4, dec/1e4
    newpath = join(outdir, newname)
    ra, dec = f"{ra:.4f}", f"{dec:.4f}"
    if exists(newpath):
        return
    _, _, colorPath = downloader(
                        log=logger,
                        settings=False,
                        downloadDirectory=outdir,
                        fits=False,
                        jpeg=True,
                        arcsecSize=10,
                        filterSet='gri',
                        color=True,
                        singleFilters=False,
                        ra=ra,
                        dec=dec,
                        imageType="stack",  # warp | stack
                        mjdStart=False,
                        mjdEnd=False,
                        window=False
                        ).get()

    jpgpath = colorPath[0]

    move(jpgpath, newpath)
    call(['convert', newpath, '-scale', '80x80', newpath])
    print('new stamp saved at', newpath)
    return newpath 

if __name__ == "__main__":
    #for success in findPositiveCandidates():
    for success in db.session.query(Candidate).filter(Candidate.humanlens>0):
        try:
          getJpgStamps(success)
          print('done for', success)
        except:
          pass
